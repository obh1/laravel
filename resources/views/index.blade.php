@extends('base')
@section('title', 'Index')
@section('content')
<a href="/todos/create" class="btn btn-primary" role="button">Create</a>
<table class="table-striped table">
<thead>
    <tr>
        <th>#</th>
        <th>Todo</th>
        <th>Visibility</th>
        <th>Operations</th>
    </tr>
</thead>
    @foreach ($todos as $todo)
        <tr>
            <td>{{ $todo->id }}</td>
            @cannot('view', $todo)
            <td>{{ $todo->title }}</td>
            @endcannot
            @can('view', $todo)
            <td><a href="/todos/{{ $todo->id }}">{{ $todo->title }}</a></td>
            @endcan
            <td><x-visibility value="{{ $todo->public }}" /></td>
            <td>
                <form action="/todos/{{ $todo->id }}" method="POST">
                    <a href="/todos/{{ $todo->id }}/edit" class="btn btn-primary" role="button">Edit</a>
                    @csrf
@method('DELETE')
<input type="submit" value="Delete" class="btn btn-danger">
</form>
            </td>
        </tr>
    @endforeach
</table>
{{ $todos->links() }}
@endsection
