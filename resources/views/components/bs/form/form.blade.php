@props(['url', 'method'])
@php
    $formMethod = $method;
    if (!in_array($method, ['POST', 'GET'])) {
        $formMethod = 'POST';
    }
@endphp
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form action="{{ $url }}" method="{{ $formMethod }}" enctype="multipart/form-data">
@csrf
@method($method)
{{ $slot }}
</form>
