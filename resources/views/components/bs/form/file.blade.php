@props(['name', 'title'])
<div class="form-group">
    <label for="form-file-{{ $name }}">{{ $title }}</label>
    <input name="{{ $name }}" type="file" class="form-control-file" id="form-file-{{ $name }}">
</div>
