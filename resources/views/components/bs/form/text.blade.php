@props(['value' => '', 'title', 'name', 'helpText' => '', 'placeholder' => ''])
@php
    $value = old($name, $value)
@endphp
<div class="form-group">
    <label for="todoTitle">{{ $title }}</label>
    <input
      class="form-control"
      name="{{ $name }}"
      id="form-input-{{ $name }}"
      value="{{ $value }}"
      aria-describedby="form-input-help-{{ $name }}"
      placeholder="{{ $placeholder }}"
    >
    @if ($slot)
    <small id="form-input-help-{{ $name }}" class="form-text text-muted">
        {{ $slot }}
    </small>
    @endif
</div>
