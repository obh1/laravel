@props(['name', 'title', 'options', 'value' => ''])
@php
    $value = old($name, $value)
@endphp
<div class="form-group">
<label for="form-radio-{{ $name }}">{{ $title }}</label>
@foreach ($options as $option)
<div class="form-check" id="form-radio-{{ $name }}">
<input name="{{ $name }}" id="form-radio-{{ $name }}-{{ $loop->index }}"
value="{{ $option['value'] }}" {{ $option['value'] == $value ? 'checked ' : '' }}
class="form-check-input" type="radio">
    <label class="form-check-label" for="form-radio-{{ $name }}-{{ $loop->index }}">
        {{ $option['title'] }}
    </label>
</div>
@endforeach
</div>
