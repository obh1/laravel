@props(['name', 'title', 'value'])
@php
    if (old($name . '-hidden') !== null) {
        $value = old($name) == 'on' ? 1 : 0;
    }
@endphp
<div class="form-check">
    <input type="hidden" name="{{ $name }}-hidden" value="hidden" />
    <input name="{{ $name }}" id="form-checkbox-{{ $name }}" {{ $value == 1 ? 'checked' : '' }} class="form-check-input" type="checkbox">
    <label class="form-check-label" for="form-checkbox-{{ $name }}">
      {{ $title }}
    </label>
  </div>
