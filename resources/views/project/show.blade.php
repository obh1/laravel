@extends('base')
@section('title', 'Show')
@section('content')
<div>{{ $project->title }}</div>
<div>Visibility: <x-visibility value="{{ $project->public }}" /></div>
<form action="/projects/{{ $project->id }}" method="POST">
<a href="/projects/{{ $project->id }}/edit" class="btn btn-primary" role="button">Edit</a>
@csrf
@method('DELETE')
<input type="submit" value="Delete" class="btn btn-danger">
</form>
@endsection
