@extends('base')
@section('title', 'Edit')
@section('content')
<x-bs.form.form url="/projects/{{ $project->id }}" method="PUT">
  <x-bs.form.text title="Title" name="title" value="{{ $project->title }}"/>
  <x-bs.form.checkbox title="Visibility" name="public" value="{{ $project->public }}" />
  <x-bs.form.submit title="Save" />
</x-bs.form.form>
@endsection
