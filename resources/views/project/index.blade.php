@extends('base')
@section('title', 'Index')
@section('content')
<a href="/projects/create" class="btn btn-primary" role="button">Create</a>
<table class="table-striped table">
<thead>
    <tr>
        <th>#</th>
        <th>Project</th>
        <th>Visibility</th>
        <th>Operations</th>
    </tr>
</thead>
    @foreach ($projects as $project)
        <tr>
            <td>{{ $project->id }}</td>
            @cannot('view', $project)
            <td>{{ $project->title }}</td>
            @endcannot
            @can('view', $project)
            <td><a href="/projects/{{ $project->id }}">{{ $project->title }}</a></td>
            @endcan
            <td><x-visibility value="{{ $project->public }}" /></td>
            <td>
                <form action="/projects/{{ $project->id }}" method="POST">
                    <a href="/projects/{{ $project->id }}/edit" class="btn btn-primary" role="button">Edit</a>
                    @csrf
@method('DELETE')
<input type="submit" value="Delete" class="btn btn-danger">
</form>
            </td>
        </tr>
    @endforeach
</table>
{{ $projects->links() }}
@endsection
