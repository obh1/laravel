@extends('base')
@section('title', 'Create New')
@section('content')
<x-bs.form.form url="/projects" method="POST">
  <x-bs.form.text title="Title" name="title" placeholder="Kenyeret venni"/>
  <x-bs.form.radios name="public" title="Visibility" :options="[['title' => 'Public', 'value' => '1'], ['title' => 'Hidden', 'value' => '0']]" />
  <x-bs.form.submit title="Create" />
</x-bs.form.form>
@endsection
