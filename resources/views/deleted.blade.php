@extends('base')
@section('title', 'Deleted')
@section('content')
<table class="table-striped table">
<thead>
    <tr>
        <th>#</th>
        <th>Todo</th>
        <th>Operations</th>
    </tr>
</thead>
    @foreach ($todos as $todo)
        <tr>
            <td>{{ $todo->id }}</td>
            <td>{{ $todo->title }}</td>
            <td>
            <form action="/todos/{{ $todo->id }}/purge" method="POST">
           @csrf
@method('DELETE')
<input type="submit" value="Purge" class="btn btn-danger">
</form>
            </td>
        </tr>
    @endforeach
</table>
{{ $todos->links() }}
@endsection