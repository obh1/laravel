@extends('base')
@section('title', 'Show')
@section('content')
{{ $todo->title }}
<form action="/todos/{{ $todo->id }}" method="POST">
<a href="/todos/{{ $todo->id }}/edit" class="btn btn-primary" role="button">Edit</a>
@csrf
@method('DELETE')
<input type="submit" value="Delete" class="btn btn-danger">
</form>
@endsection