@extends('base')
@section('title', 'Edit')
@section('content')
<x-bs.form.form url="/todos/{{ $todo->id }}" method="PUT">
  <x-bs.form.text title="Title" name="title" value="{{ $todo->title }}"/>
  <x-bs.form.submit title="Save" />
</x-bs.form.form>
@endsection
