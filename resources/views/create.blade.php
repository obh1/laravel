@extends('base')
@section('title', 'Create New')
@section('content')
<x-bs.form.form url="/todos" method="POST">
  <x-bs.form.text title="Title" name="title" placeholder="Kenyeret venni"/>
  <x-bs.form.submit title="Create" />
</x-bs.form.form>
@endsection
