@extends('base')
@section('title', 'File Upload')
@section('content')
<x-bs.form.form url="/upload" method="POST">
  <x-bs.form.file name="kep" title="Kép" />
  <x-bs.form.submit title="Upload" />
</x-bs.form.form>
@endsection
