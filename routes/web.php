<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/test', function() {
    return 'hello';
});

Route::get('/todos/deleted', 'TodoController@indexDeleted');
Route::delete('/todos/{id}/purge', 'TodoController@purge');

Route::resource('todos', 'TodoController');
Route::resource('projects', 'ProjectController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/upload', 'FileUpload@urlap');
Route::post('/upload', 'FileUpload@feldolgoz');
