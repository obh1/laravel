#!/bin/bash
docker-compose build
docker-compose up -d
if [ ! -f ".env" ]
then
    cp .env.example .env
    ./helper/composer.sh install
    ./helper/artisan.sh key:generate
    chmod -R a+w storage
fi
