FROM tanarurkerem/laravel:dev as dev
WORKDIR /project
RUN chown www-data:www-data /project
USER www-data
COPY composer.* /project/
RUN composer install --no-autoloader
COPY . /project/
RUN composer dump-autoload
USER root
RUN chmod -R a+w /project/storage
ENV LOG_CHANNEL stderr
COPY xdebug.ini	/usr/local/etc/php/conf.d

FROM tanarurkerem/laravel as live
WORKDIR /project
RUN chown www-data:www-data /project
USER www-data
COPY composer.* /project/
RUN composer install --no-dev --no-autoloader
COPY . /project/
RUN composer dump-autoload
RUN chmod u+w /project/storage
USER root
