<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Website extends Model
{
    protected $casts = [
        'head' => 'array',
    ];
}
