<?php

namespace App;

use App\Scopes\PublicProjectScope;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    public function todos()
    {
        return $this->hasMany(\App\Todo::class);
    }
     /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::addGlobalScope(new PublicProjectScope());
    }
}
