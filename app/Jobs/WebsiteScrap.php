<?php

namespace App\Jobs;

use App\Website;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class WebsiteScrap implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $website;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Website $website)
    {
        $this->website = $website;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::debug('Scraping ' . $this->website->url);
        try {
            $response = Http::get($this->website->url);
        }
        catch (Exception $e) {
            return ;
        }
        if ($response->successful()) {
            $this->website->body = $response->body();
            $this->website->head = $response->headers();
            $this->website->save();
        }
    }
}
