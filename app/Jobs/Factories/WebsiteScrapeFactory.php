<?php
namespace App\Jobs\Factories;

use App\Jobs\WebsiteScrap;
use App\Repositories\WebsiteRepository;

class WebsiteScrapeFactory
{
    private $websiteRepository;

    public function __construct(WebsiteRepository $websiteRepository)
    {
        $this->websiteRepository = $websiteRepository;
    }
    public function create($url)
    {
        $website = $this->
        websiteRepository->create($url);
        WebsiteScrap::dispatch($website);
    }
}
