<?php
namespace App\Scopes;

use App\Project;
use App\Todo;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class PublicProjectScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        if (Auth::check() && Auth::user()->can('accessUnpublished', Todo::class)) {
            return;
        }

        if ($model instanceof Project) {
            $builder->where('public', '=', 1);
        }
        if ($model instanceof Todo) {
            $builder->has('project');
        }
    }
}
