<?php
namespace App\Scopes;

use App\Todo;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

class PublicTodoScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        if (Auth::check() && Auth::user()->can('accessUnpublished', Todo::class)) {
            return;
        }
        $builder->where('public', '=', 1);
    }
}
