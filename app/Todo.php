<?php

namespace App;

use App\Scopes\PublicProjectScope;
use App\Scopes\PublicTodoScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Todo extends Model
{
    use SoftDeletes;

    public function project()
    {
        return $this->belongsTo('App\Project');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Tag');
    }
    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::addGlobalScope(new PublicTodoScope());
        static::addGlobalScope(new PublicProjectScope());
    }
}
