<?php

namespace App\Console\Commands;

use App\Jobs\Factories\WebsiteScrapeFactory;
use App\Jobs\WebsiteScrap;
use App\Repositories\WebsiteRepository;
use App\Website;
use Illuminate\Console\Command;

class WebsiteCreateJob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ws:create
                            {url* : Url of website}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a Website Job';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(WebsiteScrapeFactory $websiteScrapeFactory)
    {
        $this->info('Hello ' . join(', ', $this->argument('url')));
        foreach ($this->argument('url') as $url) {
            $websiteScrapeFactory->create($url);
        }

    }
}
