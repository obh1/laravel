<?php
namespace App\Repositories;

use App\Website;
use Illuminate\Database\QueryException;

class WebsiteRepository
{
    public function create($url) {
        try
        {
            $website = new Website();
            $website->url = $url;
            $website->save();
            return $website;
        }
        catch (QueryException $e)
        {
            return $this->getByUrl($url);
        }
    }

    public function getByUrl($url)
    {
        return Website::where('url', $url)->first();
    }
}
