<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class FileUpload extends Controller
{
    public function urlap(Request $request)
    {
        $old = $request->old('kep');
        return view('fileupload');
    }

    public function feldolgoz(Request $request)
    {
        $request->validate(['title' => 'required']);
        if ( ! $hasFile = $request->hasFile('kep')) {
            return redirect('/upload');
        }
        $file = $request->file('kep');
        $path = $file->path();
        $extension = $file->extension();
        $newPath = $file->store('public');
        $url = asset(Storage::url($newPath));
        return 'hello';
    }
}
