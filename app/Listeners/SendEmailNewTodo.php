<?php

namespace App\Listeners;

use App\Events\TodoCreated;
use App\Mail\TodoCreated as MailTodoCreated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendEmailNewTodo
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TodoCreated  $event
     * @return void
     */
    public function handle(TodoCreated $event)
    {
        Mail::to("istvan@palocz.hu")->send(new MailTodoCreated($event->todo));
    }
}
