<?php

namespace Tests\Feature;

use App\Todo;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TodoTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testList()
    {
        $response = $this->get('/todos');

        $response->assertStatus(200);
    }
    /**
     * Create users.
     *
     * @return Todo[]
     */
    private function helperMakeTodos($db) {
        $todos = [];
        for ($i = 0; $i < $db; $i++) {
            $todos[] = factory(Todo::class)->create();
        }
        return $todos;
    }
    public function testListOneTodo()
    {
        $todos = $this->helperMakeTodos(1);
        $response = $this->get('/todos');

        $response->assertStatus(200);
        $response->assertSeeText($todos[0]->title);
    }

    public function testListOnePageOfTodo()
    {
        $todos = $this->helperMakeTodos(10);
        $response = $this->get('/todos');

        $response->assertStatus(200);
        foreach($todos as $todo) {
            $response->assertSeeText($todo->title);
        }
    }
    public function testListTwoPageOfTodo()
    {
        $FirstPageOftodos = $this->helperMakeTodos(10);
        $SecondPageOftodos = $this->helperMakeTodos(10);
        $response = $this->get('/todos');

        $response->assertStatus(200);
        foreach($FirstPageOftodos as $todo) {
            $response->assertSeeText($todo->title);
        }
        foreach($SecondPageOftodos as $todo) {
            $response->assertDontSeeText($todo->title);
        }
        $response = $this->get('/todos?page=2');

        $response->assertStatus(200);
        foreach($FirstPageOftodos as $todo) {
            $response->assertDontSeeText($todo->title);
        }
        foreach($SecondPageOftodos as $todo) {
            $response->assertSeeText($todo->title);
        }
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testDeletedList()
    {
        $response = $this->get('/todos/deleted');

        $response->assertStatus(200);
    }
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCreate()
    {
        $response = $this->get('/todos/create');

        $response->assertStatus(200);
    }
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testStore()
    {
        $title = 'Test Title';
        $response = $this->followingRedirects()->post('/todos', [
            'title' => $title,
        ]);

        $response->assertStatus(200);
        $response->assertSeeText($title);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testShow()
    {
        $todo2 = factory(Todo::class)->create();
        $todo = factory(Todo::class)->create();
        $response = $this->get('/todos/' . $todo->id);
        $response->assertStatus(200);
        $response->assertSeeText($todo->title);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testEdit()
    {
        $todo = factory(Todo::class)->create();
        $response = $this->get('/todos/' . $todo->id . '/edit');
        $response->assertStatus(200);
        $response->assertSee($todo->title);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testUpdate()
    {
        $newTitle = '11111';
        $todo = factory(Todo::class)->create();
        $original = clone($todo);
        $response = $this->followingRedirects()->put('/todos/' . $todo->id, [
            'title' => $newTitle,
        ]);

        $response->assertStatus(200);
        $response->assertSeeText($newTitle);
        $response->assertDontSeeText($original->title);
    }
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testDelete()
    {
        $todo = factory(Todo::class)->create();
        $original = clone($todo);

        $listPage = $this->get('/todos');
        $listPage->assertSeeText($original->title);
        $deletedPage = $this->get('/todos/deleted');
        $deletedPage->assertDontSeeText($original->title);

        $response = $this->followingRedirects()->delete('/todos/' . $todo->id);

        $listPage = $this->get('/todos');
        $listPage->assertDontSeeText($original->title);
        $deletedPage = $this->get('/todos/deleted');
        $deletedPage->assertSeeText($original->title);

    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testPurge()
    {
        $todo = factory(Todo::class)->create();
        $original = clone($todo);

        $response = $this->followingRedirects()->delete('/todos/' . $todo->id);

        $listPage = $this->get('/todos');
        $listPage->assertDontSeeText($original->title);
        $deletedPage = $this->get('/todos/deleted');
        $deletedPage->assertSeeText($original->title);

        $response = $this->followingRedirects()->delete('/todos/' . $todo->id . '/purge');

        $listPage = $this->get('/todos');
        $listPage->assertDontSeeText($original->title);
        $deletedPage = $this->get('/todos/deleted');
        $deletedPage->assertDontSeeText($original->title);
    }

}
